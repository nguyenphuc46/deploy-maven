package com.vds.api.service;

import com.vds.api.model.Result;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AddServiceTest {
    @Test
    public void addIntegers() {
        AddService addService = new AddService();
        Result result = addService.AddService(2,5);
        Assertions.assertEquals(result.getResult(),7);
    }
}
