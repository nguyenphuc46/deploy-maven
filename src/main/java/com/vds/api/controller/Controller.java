package com.vds.api.controller;

import com.vds.api.model.Result;
import com.vds.api.service.AddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping(value = "/check-v4")
    public String checkV4(){
        return "Check Success  V4 !";
    }
    @GetMapping(value = "/check-v5")
    public String checkV5(){
        return "Check Success  V5 !";
    }
    @GetMapping(value = "/check-v6")
    public String checkV6(){
        return "Check Success   V6 !";
    }
    @GetMapping(value = "/check-v7")
    public String checkV7(){
        return "Check Success   V7 !";
    }
    @Autowired
    AddService addService;
    @GetMapping(value = "/add")
    public Result addSum(@RequestParam int a, @RequestParam int b){

        return addService.AddService(a,b);
    }
}
