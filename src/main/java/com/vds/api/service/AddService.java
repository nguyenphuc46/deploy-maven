package com.vds.api.service;

import com.vds.api.model.Result;
import org.springframework.stereotype.Service;

@Service
public class AddService {
    public Result AddService(int a, int b) {
        int sum = a + b;
        return new Result(sum);
    }
}
